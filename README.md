# Golden Layout Angular 

## Overview

This application aims to demonstrate the implementation of the golden layout in an Angular application.

## Install and run

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

