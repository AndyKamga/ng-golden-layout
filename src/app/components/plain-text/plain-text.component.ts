import { Component, ElementRef, Renderer2 } from '@angular/core';
import { GoldenLayoutItemComponent } from 'src/app/golden-layout';

@Component({
  selector: 'app-plain-text',
  templateUrl: './plain-text.component.html',
  styleUrls: ['./plain-text.component.scss'],
})
export class PlainTextComponent extends GoldenLayoutItemComponent {
  constructor(elementRef: ElementRef, rendered: Renderer2) {
    super(elementRef, rendered);
  }
}
