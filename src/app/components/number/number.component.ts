import { Component, ElementRef, Renderer2 } from '@angular/core';
import { GoldenLayoutItemComponent } from 'src/app/golden-layout';

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.scss'],
})
export class NumberComponent extends GoldenLayoutItemComponent {
  constructor(elementRef: ElementRef, rendered: Renderer2) {
    super(elementRef, rendered);
  }
}
