import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlainTextComponent, NumberComponent, ComponentID } from './components';
import { GoldenLayoutModule } from '@golden-layout/golden-layout.module';

@NgModule({
  declarations: [AppComponent, NumberComponent, PlainTextComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GoldenLayoutModule.init({
      subHiddenIds: ['navbar'],
      autoResize: true,
      itemComponents: [
        {
          id: ComponentID.PLAIN_TEXT,
          type: PlainTextComponent,
        },
        {
          id: ComponentID.NUMBER,
          type: NumberComponent,
        },
      ],
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
