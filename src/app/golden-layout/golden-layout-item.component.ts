import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive()
export abstract class GoldenLayoutItemComponent {
  public get htmlElement(): HTMLElement {
    return this.elementRef.nativeElement;
  }

  constructor(protected elementRef: ElementRef, protected rendered: Renderer2) {
    rendered.setStyle(elementRef.nativeElement, 'position', 'absolute');
    rendered.setStyle(elementRef.nativeElement, 'overflow', 'auto');
    rendered.setStyle(elementRef.nativeElement, 'height', '100%');
    rendered.setStyle(elementRef.nativeElement, 'width', '100%');
  }
}
