import {
  ComponentRef,
  Inject,
  Injectable,
  Injector,
  StaticProvider,
  Type,
  ComponentFactoryResolver,
} from '@angular/core';
import { GoldenLayoutConfig } from './golden-layout.config';
import { GoldenLayoutItemComponent } from './golden-layout-item.component';
import { ComponentContainer } from 'golden-layout';
import { ItemNotFoundError } from './golden-layout.error';
import { COMPONENT_INJECTION_TOKEN } from './golden-layout.token';

export const CONFIG_PROVIDE_TOKEN = 'config';

@Injectable()
export class GoldenLayoutService {
  public get autoResize(): boolean {
    return this.config.autoResize == undefined ? true : this.config.autoResize;
  }

  public get subHiddenIds(): string[] {
    return this.config.subHiddenIds;
  }

  private _itemComponents: Map<string, Type<GoldenLayoutItemComponent>>;

  constructor(
    @Inject(CONFIG_PROVIDE_TOKEN) private config: GoldenLayoutConfig,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    this._itemComponents = new Map<string, Type<GoldenLayoutItemComponent>>(
      config.itemComponents.map((item) => [item.id, item.type])
    );
  }

  public getItemComponentRef(
    id: string,
    container: ComponentContainer
  ): ComponentRef<GoldenLayoutItemComponent> {
    const item = this._itemComponents.get(id);
    if (!item) {
      throw new ItemNotFoundError(id);
    }

    const provider: StaticProvider = {
      provide: COMPONENT_INJECTION_TOKEN,
      useValue: container,
    };

    const componentFactoryRef =
      this.componentFactoryResolver.resolveComponentFactory<GoldenLayoutItemComponent>(
        item
      );
    return componentFactoryRef.create(
      Injector.create({
        providers: [provider],
      })
    );
  }
}
