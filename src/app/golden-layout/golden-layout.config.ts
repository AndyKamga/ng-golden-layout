import { Type } from '@angular/core';
import { GoldenLayoutItemComponent } from './golden-layout-item.component';

interface ConfiguredItemComponent {
  id: string;
  type: Type<GoldenLayoutItemComponent>;
}

export interface GoldenLayoutConfig {
  /**
   * Sub windows hidden elements IDs
   * Elements will be hide on sub window
   */
  subHiddenIds: string[];

  /**
   * Resize with container automatically
   */
  autoResize?: boolean;

  /**
   * Golden layout registered item components
   */
  itemComponents: ConfiguredItemComponent[];
}
