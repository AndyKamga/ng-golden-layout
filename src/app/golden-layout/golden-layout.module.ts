import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoldenLayoutComponent } from './golden-layout.component';
import { GoldenLayoutConfig } from './golden-layout.config';
import {
  CONFIG_PROVIDE_TOKEN,
  GoldenLayoutService,
} from './golden-layout.service';

@NgModule({
  declarations: [GoldenLayoutComponent],
  imports: [CommonModule],
  exports: [GoldenLayoutComponent],
  bootstrap: [GoldenLayoutComponent],
})
export class GoldenLayoutModule {
  public static init(
    config: GoldenLayoutConfig
  ): ModuleWithProviders<GoldenLayoutModule> {
    return {
      ngModule: GoldenLayoutModule,
      providers: [
        GoldenLayoutService,
        { provide: CONFIG_PROVIDE_TOKEN, useValue: config },
      ],
    };
  }
}
