import { InjectionToken } from '@angular/core';
import { ComponentContainer } from 'golden-layout';

export const COMPONENT_INJECTION_TOKEN = new InjectionToken<ComponentContainer>(
  'GoldenLayoutContainer'
);
