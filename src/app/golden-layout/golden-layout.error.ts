export class LayoutInitError extends Error {
  constructor(reason: string) {
    super(`Unable to init layout. ${reason}`);
  }
}

export class ComponentUnbindError extends Error {
  constructor(reason: string) {
    super(`Unable to unbind component. ${reason}`);
  }
}

export class ItemNotFoundError extends Error {
  constructor(id: string) {
    super(`Item with id ${id} not exist`);
  }
}
