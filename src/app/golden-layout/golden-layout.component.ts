import {
  ApplicationRef,
  Component,
  ComponentRef,
  ElementRef,
  EmbeddedViewRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
} from '@angular/core';
import {
  ComponentContainer,
  GoldenLayout,
  LayoutConfig,
  ResolvedComponentItemConfig,
  ResolvedLayoutConfig,
} from 'golden-layout';
import { GoldenLayoutService } from './golden-layout.service';
import { GoldenLayoutItemComponent } from './golden-layout-item.component';
import { ComponentUnbindError, LayoutInitError } from './golden-layout.error';

@Component({
  selector: 'app-golden-layout',
  template: ``,
  styles: [
    `
      :host {
        padding: 0;
        position: relative;
        display: flex;
      }
    `,
  ],
})
export class GoldenLayoutComponent implements OnInit, OnDestroy {
  @Input() public config!: LayoutConfig;
  private _componentRefs = new Map<
    ComponentContainer,
    ComponentRef<GoldenLayoutItemComponent>
  >();
  private _goldenLayout!: GoldenLayout;

  constructor(
    private _elementRef: ElementRef<HTMLElement>,
    private _layoutService: GoldenLayoutService,
    private _applicationRef: ApplicationRef,
    private _rendered: Renderer2
  ) {
    this._goldenLayout = new GoldenLayout(
      this._elementRef.nativeElement,
      this._bindComponentEventListener,
      this._unbindComponentEventListener
    );
  }

  public ngOnInit(): void {
    if (!this.config) {
      throw new LayoutInitError('Layout config missing');
    }

    this._goldenLayout.resizeWithContainerAutomatically =
      this._layoutService.autoResize;
    if (this._goldenLayout.isSubWindow) {
      this._goldenLayout.checkAddDefaultPopinButton();
      this._hideExternalSubWindowElements();
    } else {
      this._goldenLayout.loadLayout(this.config);
    }
  }

  public ngOnDestroy(): void {
    this._goldenLayout.destroy();
  }

  public getState(): ResolvedLayoutConfig {
    return this._goldenLayout.saveLayout();
  }

  public setState(state: ResolvedLayoutConfig) {
    const layoutConfig = LayoutConfig.fromResolved(state);
    this._goldenLayout.loadLayout(layoutConfig);
  }

  private _bindComponentEventListener = (
    container: ComponentContainer,
    itemConfig: ResolvedComponentItemConfig
  ) => {
    const componentRef = this._layoutService.getItemComponentRef(
      itemConfig.componentType as string,
      container
    );
    this._componentRefs.set(container, componentRef);
    this._applicationRef.attachView(componentRef.hostView);
    const htmlElement = (componentRef.hostView as EmbeddedViewRef<unknown>)
      .rootNodes[0] as HTMLElement;
    container.element.appendChild(htmlElement);

    return {
      component: componentRef.instance,
      virtual: false,
    };
  };

  private _unbindComponentEventListener = (container: ComponentContainer) => {
    const componentRef = this._componentRefs.get(container);
    if (!componentRef) {
      throw new ComponentUnbindError('Container not found');
    }

    this._componentRefs.delete(container);
    const hostView = componentRef.hostView;
    const component = componentRef.instance;
    const componentRootElement = component.htmlElement;
    container.element.removeChild(componentRootElement);
    this._applicationRef.detachView(hostView);

    componentRef.destroy();
  };

  private _hideExternalSubWindowElements(): void {
    this._rendered.setStyle(this._elementRef.nativeElement, 'height', '100%');
    this._layoutService.subHiddenIds.forEach((id) => {
      const elementToHide = document.getElementById(id);
      if (elementToHide) {
        this._rendered.setStyle(elementToHide, 'display', 'none');
      }
    });
  }
}
