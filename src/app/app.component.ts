import { Component, ViewChild } from '@angular/core';
import {
  ComponentItemConfig,
  LayoutConfig,
  ResolvedLayoutConfig,
} from 'golden-layout';
import { ComponentID } from './components';
import { GoldenLayoutComponent } from '@golden-layout/golden-layout.component';

@Component({
  selector: 'app-root',
  template: `
    <div id="navbar" class="navbar">
      <button (click)="saveState()">Save state</button>
      <button (click)="loadState()">Load state</button>
    </div>
    <app-golden-layout [config]="layouConfig"></app-golden-layout>
  `,
  styles: [
    `
      .navbar {
        background-color: purple;
        display: flex;
        align-items: center;
        justify-content: center;
        color: white;
        height: 50px;
        width: 100%;
      }
      app-golden-layout {
        width: 100%;
        height: calc(100% - 50px);
      }
      button {
        margin: 0 10px;
      }
    `,
  ],
})
export class AppComponent {
  @ViewChild(GoldenLayoutComponent) public goldenLayout!: GoldenLayoutComponent;
  private _savedState?: ResolvedLayoutConfig;

  public layouConfig: LayoutConfig = {
    root: {
      type: 'row',
      content: [
        {
          width: 80,
          type: 'column',
          content: [
            {
              title: 'Plain text',
              header: { show: 'bottom' },
              type: 'component',
              componentType: ComponentID.PLAIN_TEXT,
            },
            {
              type: 'row',
              content: [
                {
                  type: 'component',
                  title: 'Golden',
                  header: { show: 'right' },
                  isClosable: false,
                  componentType: ComponentID.NUMBER,
                  width: 30,
                  componentState: {
                    bg: 'golden_layout_spiral.png',
                  },
                } as ComponentItemConfig,
                {
                  title: 'Layout',
                  header: {
                    show: 'left',
                    popout: false,
                  },
                  type: 'component',
                  componentType: ComponentID.PLAIN_TEXT,
                  componentState: {
                    bg: 'golden_layout_text.png',
                  },
                } as ComponentItemConfig,
              ],
            },
            {
              type: 'stack',
              content: [
                {
                  type: 'component',
                  title: 'Acme, inc.',
                  componentType: ComponentID.PLAIN_TEXT,
                  componentState: {
                    companyName: 'Stock X',
                  },
                } as ComponentItemConfig,
                {
                  type: 'component',
                  title: 'LexCorp plc.',
                  componentType: ComponentID.NUMBER,
                  componentState: {
                    companyName: 'Stock Y',
                  },
                } as ComponentItemConfig,
                {
                  type: 'component',
                  title: 'Springshield plc.',
                  componentType: ComponentID.NUMBER,
                  componentState: {
                    companyName: 'Stock Z',
                  },
                } as ComponentItemConfig,
              ],
            },
          ],
        },
        {
          width: 50,
          type: 'row',
          title: 'test stack',
          content: [
            {
              type: 'stack',
              title: 'test row',
              content: [
                {
                  type: 'component',
                  title: 'comp 1',
                  componentType: ComponentID.PLAIN_TEXT,
                  componentState: {
                    companyName: 'Stock X',
                  },
                } as ComponentItemConfig,
                {
                  type: 'component',
                  title: 'comp 2',
                  componentType: ComponentID.NUMBER,
                  componentState: {
                    companyName: 'Stock Y',
                  },
                } as ComponentItemConfig,
                {
                  type: 'component',
                  title: 'comp 3',
                  componentType: ComponentID.PLAIN_TEXT,
                  componentState: {
                    companyName: 'Stock Z',
                  },
                } as ComponentItemConfig,
              ],
            },
          ],
        },
      ],
    },
  };

  public saveState(): void {
    this._savedState = this.goldenLayout.getState();
    alert('State saved !');
  }

  public loadState(): void {
    if (!this._savedState) {
      alert('STATE NOT FOUND !');
      return;
    }
    this.goldenLayout.setState(this._savedState);
  }
}
